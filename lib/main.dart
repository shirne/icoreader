import 'package:flutter/material.dart';

import 'home.dart';
import 'icon_view.dart';
import 'models/icon.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Font Reader',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: {
        '/': (BuildContext context) => const MyHomePage(title: 'Font Reader'),
        '/view': (BuildContext context) => IconViewPage(
            ModalRoute.of(context)!.settings.arguments as IconFileItem),
      },
    );
  }
}
