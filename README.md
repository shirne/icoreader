# IcoReader

可以读取IconFont图标列表的小工具.
项目原理：根据用户选择的ttf或otf文件，复制到本程序目录下，然后使用fonttools将文件转换成ttx格式，再读取内容，列出文件内的图标。

## 应用场景

1. 图标文件文档丢失，一般是diy的字体库，没有相关文档，css文件里也不能直观看到图标显示样式
2. 导入已有图标，生成对应文件(css, dart)

## [FontTools](https://pypi.org/project/fonttools/)

* 安装 Python 
* 安装 pip
* 安装 fonttools

## 平台支持 
主要考虑支持Windows,MacOS和Linux 这三个桌面端

## 功能计划
- [x] 转换字体文件为ttx并保存在同目录内
- [x] 读取ttx内的字符表
- [x] 图标列表显示优化，点击复制对应的code
- [x] 删除记录功能
- [x] 浏览图标根据关键字筛选
- [ ] 编辑图标名称
- [x] 生成css文件导出
- [x] 生成dart文件导出


## 预览图

|图标文件|图标内容|筛选和导出|
|:---:|:---:|:---:|
|![无文件](preview/01.png)|![有文件](preview/02.png)|![查看图标](preview/03.png)|

## 生成文件调用方法
提示: 生成文件可以适当修改后使用(如：生成的dart文件中变量名不合规范或与关键字冲突等)

css 文件放到与字体文件同目录下

dart文件在flutter项目配置文件 pubspec.yaml 的 flutter 节点后添加(默认项目的注释文件中有示例配置)
```
fonts:
    - family: [字体名称]
      fonts:
       - asset: assets/fonts/[字体文件].ttf
```

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
